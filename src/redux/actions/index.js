import * as types from '../constants/ActionTypes'

export const listAlbum = () => {
    return {
        type: types.LIST_ALBUM
    }
}

export const addAlbum = (genTitle, genImage) => {
    return {
        type: types.ADD_ALBUM,
        genTitle, 
        genImage
    }
}

export const addPhoto = (genImage, index) => {
    return {
        type: types.ADD_PHOTO,
        genImage, 
        index
    }
}

export const detail = (index) => {
    return {
        type: types.GO_TO_DETAIL,
        index
    }
}

export const toggleStar = (index) => {
    return {
        type: types.TOGGLE_STAR,
        index
    }
}
export const toggleHeart = (index, id) => {
    return {
        type: types.TOGGLE_HEART,
        index,
        id
    }
}
export const back = () => {
    return {
        type: types.BACK
    }
}