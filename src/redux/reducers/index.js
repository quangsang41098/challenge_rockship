import { combineReducers } from 'redux'
import listAlbum from './list-album'
import album_detail from './album-detail'

const myReducer = combineReducers({
    listAlbum: listAlbum,
    album_detail: album_detail
})

export default myReducer;