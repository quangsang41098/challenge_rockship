import * as types from '../constants/ActionTypes'
// import imageToBase64  from 'image-to-base64';

var listAlbum = JSON.parse(localStorage.getItem('listAlbum'));

var initialState = {};

var myReducer =  (state = initialState, action) => {
    switch(action.type){
        case types.GO_TO_DETAIL:
            listAlbum = JSON.parse(localStorage.getItem('listAlbum'));
            return listAlbum[action.index]
        case types.ADD_PHOTO:
            var lst =  [...listAlbum]
            lst[action.index].listPhotos.push({
                avatar: action.genImage,
                numHeart: 0
            })
            localStorage.setItem('listAlbum',JSON.stringify(lst));
            return  {...lst[action.index]}
            // break;
        case types.TOGGLE_STAR:
            var lst_st = [...listAlbum]
            lst_st[action.index].star = !lst_st[action.index].star
            localStorage.setItem('listAlbum',JSON.stringify(lst_st));
            return  {...lst_st[action.index]}
        case types.TOGGLE_HEART:
            var lst_heart = [...listAlbum]
            lst_heart[action.id].listPhotos[action.index].numHeart++
            localStorage.setItem('listAlbum',JSON.stringify(lst_heart));
            return  {...lst_heart[action.id]}
        default:
            return state
    }
    // return {...lst[action.index]}
}

export default myReducer;