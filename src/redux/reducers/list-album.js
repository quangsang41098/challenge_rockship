import * as types from '../constants/ActionTypes'


var listAlbum = JSON.parse(localStorage.getItem('listAlbum'));

var initialState = listAlbum ? listAlbum : [];

var myReducer = (state = initialState, action) => {
    switch(action.type){
        case types.LIST_ALBUM:
            return state
        case types.ADD_ALBUM:
            var d = new Date()
            state.push({
                id: state.length,
                title: action.genTitle,
                avatar: action.genImage,
                listPhotos: [],
                star: false,
                date: "" + d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear()
            })
            localStorage.setItem('listAlbum',JSON.stringify(state));
            return [...state]
        case types.TOGGLE_STAR:
            var lst = state
            lst[action.index].star = !lst[action.index].star
            localStorage.setItem('listAlbum',JSON.stringify(lst));
            return [...lst]
        case types.BACK:
            listAlbum = JSON.parse(localStorage.getItem('listAlbum'));
            return [...listAlbum]
        // case types.GO_TO_DETAIL:
        //     return [...listAlbum]
        default:
            return state
    }
}

export default myReducer;