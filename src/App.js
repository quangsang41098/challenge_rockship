import React from 'react';
import ListAlbum from './layout/ListAlbum';
import AlbumDetail from './layout/AlbumDetail';
// import faker from 'faker';
// import imageToBase64  from 'image-to-base64';
import './App.css';
import 'bulma/css/bulma.css'

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //listAlbum: [],
      isListAlbum: true,
      isAlbumDetail: false,
     // album: ""
    }
  }
  // componentWillMount() {
  //   if (localStorage && localStorage.getItem('listAlbum')){
  //     var listAlbum = JSON.parse(localStorage.getItem('listAlbum'));
  //     this.setState({listAlbum: listAlbum});
  //   }
  // }
  // addAlbum = (genTitle, genImage) => {
  //   var lst = this.state.listAlbum
  //   var d = new Date()
  //   lst.push({
  //     id: this.state.listAlbum.length,
  //     title: genTitle,
  //     avatar: genImage,
  //     listPhotos: [],
  //     star: false,
  //     date: "" + d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear()
  //   })
  //   this.setState({listAlbum: lst})
  //   localStorage.setItem('listAlbum',JSON.stringify(lst));
  // }
  // addPhoto = async ( genImage, index) => {
  //   var lst = this.state.listAlbum
  //   var imgBase64 = async () => imageToBase64(genImage)
  //       .then(
  //           (response) => {
  //               return response
  //           }
  //       )
  //   lst[index].listPhotos.push({
  //     avatar: genImage,
  //     imgBase64: await imgBase64(),
  //     numHeart: 0
  //   })
  //   this.setState({listAlbum: lst})
  //   localStorage.setItem('listAlbum',JSON.stringify(lst));
  // }
  gotoDetail = (index) => {
    this.setState({
      isListAlbum: false,
      isAlbumDetail: true,
      // album: this.state.listAlbum[index]
    })
  }
  backList = () => {
    this.setState({
      isListAlbum: true,
      isAlbumDetail: false
    })
  }
  // toggleStar = (i) => {
  //   var lst = this.state.listAlbum
  //   lst[i].star = !lst[i].star
  //   this.setState({listAlbum: lst})
  //   localStorage.setItem('listAlbum',JSON.stringify(lst));
  // }
  // toggleHeart = (i, iAlbum) => {
  //   var lst = this.state.listAlbum
  //   lst[iAlbum].listPhotos[i].numHeart++
  //   this.setState({listAlbum: lst})
  //   localStorage.setItem('listAlbum',JSON.stringify(lst));
  // }
  render(){
      const { isAlbumDetail, isListAlbum } = this.state
      return(
        <div className="App">
          {isListAlbum && <ListAlbum 
                            //listAlbum = {listAlbum} 
                            //addAlbum = { this.addAlbum}
                            gotoDetail = { this.gotoDetail }
                            //toggleStar = {this.toggleStar} 
                          />}
          {isAlbumDetail && <AlbumDetail  //album = { album }
                                          //addPhoto = { this.addPhoto}
                                          backList = { this.backList }
                                          // toggleStar = {this.toggleStar} 
                                          // toggleHeart = { this.toggleHeart }
                                           />}

        </div>
      )
  }
}

export default App;
