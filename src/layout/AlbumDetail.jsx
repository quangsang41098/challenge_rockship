import React from 'react';
import '../css/album-detail.css';
import faker from 'faker'
import { connect } from 'react-redux'
import * as actions from '../redux/actions/index'


class AlbumDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggleAddPhoto : false,
            toggleLightBox : false,
            genImage: "",
            imgLightBox: ""
        }
    }
    toggleAddPhoto = () => {
        this.setState({toggleAddPhoto : !this.state.toggleAddPhoto})
    }
    toggleLightBox = (img) => {
        this.setState({
            toggleLightBox : !this.state.toggleLightBox,
            imgLightBox: img
        })
    }
    genImage = () => {
        this.setState({genImage: faker.image.avatar()})
    }
    addPhoto = (index) => {
        const { genImage} = this.state
        this.props.addPhoto( genImage, index )
        this.toggleAddPhoto()
    }
    back = () => {
        this.props.back()
        this.props.backList()
    }
    render(){
        const { toggleAddPhoto,toggleLightBox, genImage } = this.state
        const { album_detail } = this.props
        const classnameModal = toggleAddPhoto ? "modal is-active" : "modal"
        const classnameLightBox = toggleLightBox ? "modal is-active" : "modal"
        return(
            <div className="container">
                <button className="button is-primary" onClick={this.back}>Back</button>
                <div className="box header">
                    <div className="content">
                        <div className="left">
                            <h4><strong>{album_detail.title}</strong></h4>
                            <p style={{color: "gray"}}>{album_detail.date}</p>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                                Iste nobis, accusantium doloribus distinctio optio et ipsam, reprehenderit alias eius excepturi quas! Vero accusamus esse, in illum eum impedit soluta dicta.</p>
                        </div>
                        <div className="right">
                            <div className="right-top">
                                <div className={album_detail.star ? "star star-true" : "star"}>
                                    <span onClick={() => this.props.toggleStar(album_detail.id)}><i className={album_detail.star ? "fa fa-star":"fa fa-star-o"} aria-hidden="true"></i></span>

                                </div>
                                <div className="three-dot">
                                    <span><i className="fa fa-ellipsis-v" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <div className="right-bottom">
                                <p style={{color: "gray"}}>{album_detail.listPhotos?.length} photos</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="box main">
                <div className="items">
                    <div className="item">
                        <div className="box box-photo">
                            <article className="media">
                                <div className="media-top" onClick={ this.toggleAddPhoto }>
                                    <figure className="create-image">
                                        <i className="fa fa-plus-circle" aria-hidden="true"></i>
                                    </figure>
                                </div>
                                <div className="media-bot">
                                    <h4>Add Photo</h4>
                                </div>
                            </article>
                        </div>
                                
                        <div className={classnameModal}>
                            <div className="modal-background"></div>
                            <div className="modal-content">
                                <div className="gen-image">
                                    <button className="button is-danger" onClick={ this.genImage }>Generate Image</button>
                                    <img src={ genImage } alt=""/>
                                </div>
                                <div className="save">
                                    <button className="button is-success" onClick={ () => this.addPhoto(album_detail.id) }>Add Photo</button>
                                </div>
                            </div>
                            <button className="modal-close is-large" aria-label="close" onClick={ this.toggleAddPhoto}></button>
                        </div>
                    </div>
                    {album_detail.listPhotos.map((ele, index) => {
                        return(
                            <div className="item" key={index}>
                                <div className="box box-photo">
                                    <article className="media">
                                        <div className="media-top" onClick={ () =>  this.toggleLightBox(ele.avatar) }>
                                            <figure className="image">
                                                <img src={ele.avatar} alt=""/>
                                            </figure> 
                                        </div>
                                        <div className="media-bot-item">
                                            <div className={ele.numHeart === 0 ? "overlay": "overlay heart-on"}>
                                                <span onClick={() => this.props.toggleHeart(index, album_detail.id)}><i className={ele.numHeart === 0 ? "fa fa-heart-o" : "fa fa-heart"} aria-hidden="true"></i> {ele.numHeart}</span>

                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        )
                    })}
                    <div className={classnameLightBox}>
                        <div className="modal-background">
                            <div className="modal-content">
                                <figure className="image" style={{marginTop: "100px", marginLeft: "25%"}}>
                                    <img src={this.state.imgLightBox} alt=""/>
                                </figure> 
                                <button className="modal-close is-large" aria-label="close" onClick={() =>  this.toggleLightBox()}></button>
                            </div>
                        </div>
                        
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        listAlbum: state.listAlbum,
        album_detail: state.album_detail
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        addPhoto: (genImage, index ) => {
            dispatch(actions.addPhoto(genImage, index ))
        },
        toggleStar: (index) => {
            dispatch(actions.toggleStar(index))
        },
        toggleHeart: (index, id) => {
            dispatch(actions.toggleHeart(index, id))
        },
        back: () => {
            dispatch(actions.back())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AlbumDetail);