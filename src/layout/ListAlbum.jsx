import React from 'react';
import '../css/list-album.css'
import faker from 'faker'
import { connect } from 'react-redux'
import * as actions from '../redux/actions/index'
class ListAlbum extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggleAddAlbum : false,
            genTitle: "",
            genImage: ""
        }
    }
    toggleAddAlbum = () => {
        this.setState({toggleAddAlbum : !this.state.toggleAddAlbum})
    }
    toggleStar = (i) => {
        this.props.toggleStar(i)
    }

    genTitle = () => {
        this.setState({genTitle: faker.lorem.sentence()})
    }
    genImage = () => {
        this.setState({genImage: faker.image.avatar()})
    }
    addAlbum = () => {
        const {genTitle, genImage} = this.state
        this.props.addAlbum(genTitle, genImage)
        this.toggleAddAlbum()
    }
    gotoDetail = (index) => {
        this.props.detail(index)
        this.props.gotoDetail(index)
    }
    render(){
        const { toggleAddAlbum, genTitle, genImage } = this.state
        const { listAlbum } = this.props
        const classnameModal = toggleAddAlbum ? "modal is-active" : "modal"
        return(
            <div className="container">
                {/* <button className="button is-primary is-large modal-button" data-target="modal-ter" aria-haspopup="true">Launch card modal</button> */}
                <div className={classnameModal}>
                    <div className="modal-background"></div>
                    <div className="modal-content">
                        <div className="gen-title">
                            <button className="button is-danger" onClick={ this.genTitle }>Generate Title</button>
                            <input className="input" type="text" placeholder="Text input" value={ genTitle } readOnly></input>
                            {/* <img src={faker.image.avatar()} alt=""/> */}
                        </div>
                        <div className="gen-image">
                            <button className="button is-danger" onClick={ this.genImage }>Generate Image</button>
                            <img src={ genImage } alt=""/>
                        </div>
                        <div className="save">
                            <button className="button is-success" onClick={ this.addAlbum }>Add Album</button>
                        </div>
                    </div>
                    <button className="modal-close is-large" aria-label="close" onClick={ this.toggleAddAlbum}></button>
                </div>
                <nav className="breadcrumb has-dot-separator" aria-label="breadcrumbs">
                    <ul className="title">
                        <li className="album">Album({listAlbum.length})</li>
                    </ul>
                </nav>
                <div className="items">
                    <div className="item">
                        <div className="box">
                            <article className="media">
                                <div className="media-top" onClick={ this.toggleAddAlbum}>
                                    <figure className="create-image">
                                        <i className="fa fa-plus-circle" aria-hidden="true"></i>
                                    </figure>
                                </div>
                                <div className="media-bot-add">
                                    <h4>Create New Album</h4>
                                </div>
                            </article>
                        </div>
                        
                    </div>
                    {listAlbum.map((ele, index) => {
                        return (
                            <div className="item" key={index}>
                                <div className="box">
                                    <article className="media">
                                        <div className="media-top" onClick={ () => this.gotoDetail(index) }>
                                            <figure className="image">
                                                <img src={ele.avatar} alt=""/>
                                            </figure>
                                        </div>
                                        <div className="media-bot" onClick={ () => this.gotoDetail(index) }>
                                            <h4 style={{cursor: "pointer"}}><strong>{ele.title}</strong></h4>
                                            <p>{ele.listPhotos.length} photos</p>
                                        </div>
                                        <div className={ele.star ? "star star-true" : "star"}>
                                            <span onClick={ () => this.toggleStar(index) }><i className={ele.star ? "fa fa-star":"fa fa-star-o"} aria-hidden="true"></i></span>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        )
                    })}
                    
                    
                </div>
          </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        listAlbum: state.listAlbum
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        addAlbum: (genTitle, genImage) => {
            dispatch(actions.addAlbum(genTitle, genImage))
        },
        detail: (index) => {
            dispatch(actions.detail(index))
        },
        toggleStar: (index) => {
            dispatch(actions.toggleStar(index))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListAlbum);